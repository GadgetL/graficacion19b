#include <glut.h>
#include<windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#define M_PI 3.14159265358979323846
using namespace std;
bool verEjes = false;
double rotate_y=0;
double rotate_x=0;
double rotate_z=0;

GLfloat X = 0.0f;
GLfloat Y = 0.0f;
GLfloat Z = 0.0f;
GLfloat X1 = 0.0f;
GLfloat Y1 = 0.0f;
GLfloat Z1 = 0.0f;
float scale = 1.0f;

bool bx=false;
bool by=false;
bool bz=false;

void init(void) {
	glClearColor(0.0, 0.0, 0.0, 0.0);
}

void ArrowKey(int key, int x, int y) {
	//C�digo
	switch(key){
	case GLUT_KEY_RIGHT:
		X+=1.0;
		break;
		//  Flecha izquierda: disminuir rotaci�n 5 grados
	case GLUT_KEY_LEFT:
		X-= 1.0;
		break;
	case GLUT_KEY_UP:
		Y+= 1.0;
		break;
	case GLUT_KEY_DOWN:
		Y-= 1.0;
		break;
	case GLUT_KEY_HOME:
		scale+=0.5;
		break;
	case GLUT_KEY_END:
		scale+=-0.5;
		break;
	case 65:
		X1+=1.0;
	break;
	case 66:
		X1-=1.0;
	break;
	case 73:
		Y1+= 1.0;
		break;
	case 68:
		Y1-= 1.0;
		break;
	}
	glutPostRedisplay();
}
/*
void dibujaCubo(){
	//LADO FRONTAL: lado multicolor
	glBegin(GL_POLYGON);
	glTranslatef(2.0-X1,2.0-Y1,1.5-Z1);
	glColor3f( 1.0, 2.0, 0.0 );     
	glVertex3f(  0.5, -0.5, -0.5 );      // P1 es rojo
	//glColor3f( 0.0, 1.0, 0.0 );
	glVertex3f(  0.5,  0.5, -0.5 );      // P2 es verde
	//glColor3f( 0.0, 0.0, 1.0 );
	glVertex3f( -0.5,  0.5, -0.5 );      // P3 es azul
	//glColor3f( 1.0, 0.0, 1.0 );
	glVertex3f( -0.5, -0.5, -0.5 );      // P4 es morado
	glEnd();
	//gluLookAt(2.0-X1, 2.0-Y1, 6.5-Z1, 2.0-X1, 2.0-Y1, 2.0-Z1, 0.0, 1.0, 0.0);
}*/

void dibujarTriangulo(){
	glPushMatrix();
	glTranslatef(1.0-X,1.2-Y,1.2-Z);
	glBegin(GL_TRIANGLES);
	glColor3f( 1.0, 0.0, 3.0 );     
	glVertex3f(  1.97, -1.5, -1.5 );      // P1 es rojo
	//glColor3f( 0.0, 1.0, 0.0 );
	glVertex3f(  1.7,  1.7, -1.7 );      // P2 es verde
	//glColor3f( 0.0, 0.0, 1.0 );
	glVertex3f( 1,  1.7, -1.7 );      // P3 es azul
	//glColor3f( 1.0, 0.0, 1.0 );	
	glEnd();
	glPopMatrix();
}

void display(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	//gluLookAt(0.0-X, 0.0-Y, 4.5-Z, 0.0-X, 0.0-Y, 0.0-Z, 0.0, 1.0, 0.0);
	glPushMatrix();
	if(bx==true){
		rotate_x += .9;
	}
	if(by==true){
		rotate_y += .9;
	}
	if(bz==true){
		rotate_z += .9;
	}
	glRotatef( rotate_x, 1.0, 0.0, 0.0 );
	glRotatef( rotate_y, 0.0, 1.0, 0.0 );
	glRotatef( rotate_z, 0.0, 0.0, 1.0 );
	glScalef(scale, scale, scale);
	dibujarTriangulo();
//	dibujaCubo();
	glPopMatrix();
	glFlush();
	glutSwapBuffers();
}

void reshape(int w, int h) {
	glViewport(0, 0, (GLsizei)w, (GLsizei)h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-10.0, 10.0, -10.00, 10.0, 0.1, 20.0);
	glMatrixMode(GL_MODELVIEW);
}

void keyboard(unsigned char key, int x, int y) {
	switch (key){
	case 43:
		Z+=1.0;
		break;
	case 45:
		Z-=1.0;
		break;
	case 40:
		Z1+=1.0;
		break;
	case 41:
		Z1-=1.0;
		break;
	case 120:
		if(by==false){
			by=true;
		}
		else{
			by=false;
		}
		break;
	case 121:
		if(bx==false){
			bx=true;
		}
		else{
			bx=false;
		}
		break;
	case 122:
		if(bz==false){
			bz=true;
		}
		else{
			bz=false;
		}
		break;
	case 97:
		verEjes = !verEjes ? true : false;
	break;
	case 27:
		exit(0);
	break;
	}
}
/*
int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(400, 400);

	glutInitWindowPosition(100, 100);
	glutCreateWindow("Cubo 3D");
	glEnable(GL_DEPTH_TEST);
	init();

	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutIdleFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(ArrowKey);
	glutMainLoop();
	return 0;
}*/
