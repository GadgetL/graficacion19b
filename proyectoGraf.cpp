#include <glut.h>
#include <stdlib.h>
#include "iostream"
#include "stdafx.h"
#include "main.h"
#include "ImageLoader.h"
#include <math.h>


GLuint _texturaA; //Variable para almacenar textura
//funciones
void cuadro(float x, float y);

float eyex = -16;
float eyey = 5;
float eyez = -14;

/** Rotar Camara*/
float centerx = 0.0f;
float centery = 0.2f;
float centerz = 0.0f;
/** Levantar camara*/
float upx = 0.0f;
float upy = 2.0f;
float upz = 1.0f;
int moverZ=2;
int moverX=0;
int moverY=0;
#define DEGREES_PER_PIXEL	.8f
#define UNITS_PER_PIXEL		.1f
#define ZOOM_FACTOR		.04f

/*Angulos de rotacion de escena - interactivo*/
float xRotate = 0, yRotate = 40;

int xmatriz=6;
int ymatriz=0;

int mapaPasillos[35][28] = {
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0},
{0,0,0,0,0,0,1,1,1,0,0,1,1,1,1,1,1,0,0,1,1,1,0,0,0,0,0,0}};

/** Inician Elementos para la rotacion y el movimiento del mouse*/
#define UPPER_ARM_HEIGHT  3.0//3.0
#define LOWER_ARM_HEIGHT  2.0//2.0
#define UPPER_ARM_RADIUS  .5//.5
#define LOWER_ARM_RADIUS  .5//.5
const int AXIS_SIZE = 3000;
 
typedef struct {
  bool leftButton;
  bool rightButton;
  int x;
  int y;
} MouseState;

MouseState mouseState = { false, false, 0, 0 };

GLuint loadTexture(Image* image) {
	GLuint idtextura;
	glGenTextures(1, &idtextura);
	glBindTexture(GL_TEXTURE_2D, idtextura);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	return idtextura;
}

//cargar una imagen bmp en un apuntador, que se convierte en una variable de tipo entero GL
//y se elimina el apuntador
void initRendering() {
	Image* lado1 = loadBMP("6.bmp");
	_texturaA = loadTexture(lado1);
	delete lado1;

}

void cargarTextura(GLuint _textura) {
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, _textura);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
}

void cuadro(float x, float y){
	cargarTextura(_texturaA);
	glBegin(GL_QUADS);		
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(x, y);
		glTexCoord2f(1.0f, 0.0f);
		glVertex2f(x + 1, y);
		glTexCoord2f(1.0f, 1.0f);
		glVertex2f(x+1, y+1);
		glTexCoord2f(0.0f, 1.0f);
		glVertex2f(x, y+1);
	glEnd();	
}



void myInit() {
  glClearColor(109,183,247,1);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(120.0, 1.0, 0.1, -10.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glEnable(GL_DEPTH_TEST);
}

void dibujaCubo(){
	glPushMatrix();
	glBegin(GL_POLYGON);
	glTranslatef(1.0,1.2,1.2);    
	glColor3f( 1.0, 0.0, 3.0 ); 
	glVertex3f(  0.5, -0.5, -0.5 );      // P1 es rojo
	glColor3f( 0.0, 1.0, 0.0 );
	glVertex3f(  0.5,  0.5, -0.5 );      // P2 es verde
	glColor3f( 0.0, 0.0, 1.0 );
	glVertex3f( -0.5,  0.5, -0.5 );      // P3 es azul
	glColor3f( 1.0, 0.0, 1.0 );
	glVertex3f( -0.5, -0.5, -0.5 );      // P4 es morado
	gluLookAt(0.0, 0.0, 4.5, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glEnd();
	glPopMatrix();
}
//Dibuja cono
void cono(int x, int y, int z,int r,int h, int s,int o){
	//glClear(GL_COLOR_BUFFER_BIT);
	//glMatrixMode(GL_MODELVIEW);
	
    glPushMatrix();                   // roof
	glTranslatef(x,y,z);
	glScalef(1,3,3);
    glRotatef(-90,1,0,0);
	glutSolidCone(r,h,s,o);
    //glutSolidCone(1.5,1,16,8);
    glPopMatrix();
}
void dibujaCuadros(double x, double y, double z,double ancho, double alto,double largo,double r,double g, double b){
	glColor3d(r,g,b);
	glPushMatrix();
	glTranslated(x,y,z);	//x, y ,z 
	glScalef(ancho,alto,largo);	// ancho alto y largo 
	glutSolidCube(1);	// hace el cubo
	glPopMatrix();
}

void areasverdes(){
	glColor3d(1.0, 0.0, 1.0);
    glPushMatrix();
        glTranslated(0,-0.01,-17);
        glScalef(800,-.6,120);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-16,-0.01,-9);
        glScalef(160,-.6,120);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(15,-0.01,-10);
        glScalef(200,-.6,160);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-18,-0.01,4);
        glScalef(80,-.6,80);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-13,-0.01,8);
        glScalef(80,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-8,-0.01,2.5);
        glScalef(80,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-7,-0.01,3.5);
        glScalef(40,-.6,60);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-10.5,-0.01,4);
        glScalef(60,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-9.5,-0.01,7.5);
        glScalef(20,-.6,60);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-6.5,-0.01,9);
        glScalef(20,-.6,100);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-8,-0.01,9.5);
        glScalef(80,-.6,60);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-1.5,-0.01,16);
        glScalef(80,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(1.5,-0.01,18.5);
        glScalef(20,-.6,20);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(5.5,-0.01,18);
        glScalef(20,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(4,-0.01,15.5);
        glScalef(200,-.6,60);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(14,-0.01,12);
        glScalef(120,-.6,80);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-0.5,-0.01,5);
        glScalef(20,-.6,20);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(1.5,-0.01,5);
        glScalef(20,-.6,20);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(0.5,-0.01,7.5);
        glScalef(60,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-1,-0.01,0);
        glScalef(40,-.6,120);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(2,-0.01,-1);
        glScalef(80,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(3.5,-0.01,1.5);
        glScalef(20,-.6,60);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(3,-0.01,-6);
        glScalef(40,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(-2.5,-0.01,10.5);
        glScalef(20,-.6,60);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(11,-0.01,7);
        glScalef(40,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(6.5,-0.01,7);
        glScalef(20,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(4.5,-0.01,6.5);
        glScalef(60,-.6,20);
        glutSolidCube(.05);
    glPopMatrix();
    glPushMatrix();
        glTranslated(3,-0.01,-9);
        glScalef(40,-.6,40);
        glutSolidCube(.05);
    glPopMatrix();
}

void pilar(){
	glColor3d(1.0, 0.0, 1.0);
    glPushMatrix();
		GLUquadricObj *quadratic; 
		quadratic = gluNewQuadric(); 
		//posicion x y z
        glTranslated(0,-0.01,-17);
		//ancho alto largo
        glScalef(8,15,4);
		gluCylinder(quadratic,0.1f,0.1f,0.5f,30,30);
		//glutSolidCube(1);
    glPopMatrix();


}

void edificioPrueba(){
	glColor3d(1.0, 0.0, 1.0);
    glPushMatrix();
		GLUquadricObj *quadratic; 
		quadratic = gluNewQuadric(); 
		//posicion x y z
        glTranslated(0,-0.01,-17);
		//ancho alto largo
        glScalef(8,15,4);
		//gluCylinder(quadratic,0.1f,0.1f,0.5f,30,30);
		//glutSolidCube(1);
    glPopMatrix();
}

void edificios(){
	//Pilares entredas
	
	dibujaCuadros(-22,2,-16,1,5,1,0,0,0);
	dibujaCuadros(-22,2,-10,1,13,1,0,0,0);
	dibujaCuadros(-22,2,1,1,13,1,0,0,0);
	dibujaCuadros(-22,2,7,1,5,1,0,0,0);

	//torres
	dibujaCuadros(-22,2,-18.5,1,30,3,0,0,0);
	dibujaCuadros(-22,2,8.5,1,30,3,0,0,0);
	//techo entrada
	dibujaCuadros(-22,6.5,-13,1,4,7,.2,.2,.3);
	dibujaCuadros(-22,8,-4,1,1,11,.2,.2,.3);
	dibujaCuadros(-22,6.5,4,1,4,7,.2,.2,.3);
	dibujaCuadros(-22,10.8,-4.5,1,4,24,.2,.2,.3);
	//paredes
	dibujaCuadros(-2,9,7,    39,7,2,1,.0,0);
	dibujaCuadros(-2,9,-15.5,39,7,2,1,0,0);

	dibujaCuadros(17,5,-4,1,14.5,24.5,.2,.2,.3);
	dibujaCuadros(17,13.5,-4,1,2.5,3.5,0,0,1);

	//viga
	dibujaCuadros(-2,11,-7.5,40,1,2,.2,.2,.3);

	//postes
	for (int i = -15; i <14;){
		dibujaCuadros(i,2,-7.5,1,18,1,1,0,0);
		i+=3;
	}

	for (int i = -20; i < 16; i++){
		dibujaCuadros(i,2,7,2,13,1,1,0,0);
		i+=5;
	}

	for (int i = -20; i <= 16; i++){
		dibujaCuadros(i,2,-16,2,13,1,1,0,0);
		i+=5;
	}	
	
}


void pilar2(float cx, float cy, float cz){
	glColor3d(cx,cy,cz);
	glPushMatrix();
		GLUquadricObj *quadratic; 
		quadratic = gluNewQuadric(); 
		//posicion x y z
        glTranslated(0,-0.01,-17);
		//ancho alto largo
        glScalef(8,15,4);
		glutSolidCube(1);
		gluCylinder(quadratic,0.5f,0.8f,0.5f,40,30);
		
	glPopMatrix();
}




void Display(void) {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  /*Reposicionar camara */
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
    gluLookAt(eyex,eyey,eyez,0,0,0 ,0,1,0);
    glTranslated(eyex, 0, eyez);
    glRotatef(xRotate, 0, 1, 0);
    glTranslated(-eyex, 0, -eyez);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_TEXTURE_2D);
    glColor3d(0.5, 1.0, 0.5);
    glPushMatrix();
	
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glBegin(GL_QUADS);
            glTexCoord3f(0.0,1.0,0.0);  glVertex3f(-25,0,25);
			glTexCoord3f(0.0,1.0,0.0);  glVertex3f(25,0,25);
			glTexCoord3f(0.0,1.0,0.0);  glVertex3f(25,0,-25);
			glTexCoord3f(0.0,1.0,0.0);  glVertex3f(-25,0,-25);
        glEnd();

		//dibujacono();
	edificios();
	
	glPopMatrix();
	cono(-22.8,17,9,1.5,1,16,10);
	cono(-22.8,17,-18.5,1.5,1,16,10);
	glFlush();
  glutSwapBuffers();
}

 void key(unsigned char key, int x, int y){
    switch (key){
        case 27 :
        case 'q':
            exit(0);
            break;
        case '1':
                eyex = -13;
				eyey = 2;
				eyez = -13;
				centerx = 0.0;
				centery = 0.2;
				centerz = 1.0;
				upx = 0;
				upy = 2.0;
				upz = 1.0;
        break;
        case '2':
				eyex = 0.5;
				eyey = 2.2;
				eyez = 0.5;
				centerx = 0.0;
				centery = 0.2;
				centerz = 1.0;
				upx = 0;
				upy = 5.0;
				upz = 1.0;
        break;
		case '3':
				eyex = 0;
				eyey = 25;
				eyez = -30;
				centerx = 0;
				centery = 8;
				centerz = 0;
				upx = 0;
				upy = .2;
				upz = 1;
        break;
		case 's':  
            if(ymatriz>0){
            if(mapaPasillos[ymatriz-1][xmatriz]==1){
            eyez=eyez-1;
            centerz=centerz-1;
            ymatriz--;
            }
            }
            break;

        case 'd':
            if(xmatriz>0){
                if(mapaPasillos[ymatriz][xmatriz-1]==1){
                 eyex=eyex-1;
                 centerx=centerx-1;
                 xmatriz--;
            }
            }
            break;
        case 'w':
            if(ymatriz<28){
                if(mapaPasillos[ymatriz+1][xmatriz]==1){
                 eyez=eyez+1;
                 centerz=centerz+1;
                 ymatriz++;;
                }
            }
        break;
        case 'a':
            if(xmatriz<28){
                if(mapaPasillos[ymatriz][xmatriz+1]==1){
                 eyex=eyex+1;
                 centerx=centerx+1;
                xmatriz++;
            }
            }
        break;
    }
    glutPostRedisplay();
}
 
void Mouse(int button, int state, int x, int y) {
 
  if (button == GLUT_LEFT_BUTTON) {
    if (state == GLUT_DOWN)
      mouseState.leftButton = true;
    else
      mouseState.leftButton = false;
  }
  if (button == GLUT_RIGHT_BUTTON) {
    if (state == GLUT_DOWN)
      mouseState.rightButton = true;
    else
      mouseState.rightButton = false;
  }
  
  mouseState.x = x;
  mouseState.y = y;
}

void MouseMove(int x, int y) {
  int xDelta = mouseState.x - x;
  int yDelta = mouseState.y - y;
  
  mouseState.x = x;
  mouseState.y = y;

 
  if (mouseState.leftButton) {
    xRotate -= xDelta * DEGREES_PER_PIXEL;
    yRotate -= yDelta * DEGREES_PER_PIXEL;
  }
  glutPostRedisplay();
}

int main(int argc, char **argv) {
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(900,600);
  glutCreateWindow("Graficacion");
  glutDisplayFunc(Display);
  glutMouseFunc(Mouse);
  glutMotionFunc(MouseMove);
  myInit();
  glutKeyboardFunc(key);

  /* set up depth-buffering */
    //glEnable(GL_DEPTH_TEST);

    /* turn on default lighting */
    //glEnable(GL_LIGHTING);
    //glEnable(GL_LIGHT0);

    /* define the projection transformation */
    //glMatrixMode(GL_PROJECTION);
    //glLoadIdentity();
    //gluPerspective(40,1,2,20);

    /* define the viewing transformation */
    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();
  glutMainLoop();
}